
# spectral-baseline

**Table of Contents**

[TOC]


-----

GUI based tool for analyzing baseline of a Raman spectrum using the `derpsalsa` algorithm described by Vitaly I. Korepanov in https://doi.org/10.1002/jrs.5952 and in the [repository](https://github.com/the-different-name/spectral_baseline).

All citation should go to the above article.

## Usage
In order to run the GUI, download the installer (in the dist directory), proceed with installation and use the shortcut created on the desktop or menu.

This is the suggested approach as no extra step is needed.


## GUI Layout

<img src="/img/window_example_.PNG" width="575">


## For developers

For those interested in development, run the python file in src directory as

`python DAT_baseline.py`

### Requirements

`numpy`
`scipy`
`matplotlib`

`igor`
`igorwriter`
`PyQt5`

### Installing required modules

`pip install numpy scipy matplotlib` or `conda install numpy scipy matplotlib`

`pip install igor igorwriter PyQt5`

-----

For more details see baseline subtraction algorithms described in the JRS paper: https://doi.org/10.1002/jrs.5952

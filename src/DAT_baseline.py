
import os
from numpy import arange, ndarray, ndim
from PyQt5 import QtCore, QtGui, QtWidgets

#######################################################
print('\t\t loading...\n\n')
#######################################################

from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as Canvas
import matplotlib

# Ensure using PyQt5 backend
matplotlib.use('QT5Agg')

# Matplotlib canvas class to create figure
class MplCanvas(Canvas):
    def __init__(self):
        self.fig = Figure()
        self.ax = self.fig.add_subplot(111)
        Canvas.__init__(self, self.fig)
        Canvas.setSizePolicy(self, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        Canvas.updateGeometry(self)

# Matplotlib widget
class MplWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        QtWidgets.QWidget.__init__(self, parent)   # Inherit from QWidget
        self.canvas = MplCanvas()                  # Create canvas object
        self.vbl = QtWidgets.QVBoxLayout()         # Set box for plotting
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)


############################################################################################
from numpy import  loadtxt, savetxt, save, load
from igor import binarywave
from igorwriter import IgorWave

############################################################
def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)
############################################################

# load file as numpy object ################################

def load_data(file_address):
    if file_address.endswith('.txt'):

        data = loadtxt(file_address)
        print('\tFile type \t: txt file')
        print('\tDimension \t:', data.shape)
        return data

    elif file_address.endswith('.ibw'):

        print('\tFile type \t: igor binary wave')
        data=binarywave.load(file_address)['wave']['wData']
        print('\tDimension \t:', data.shape)
        return data


    elif file_address.endswith('.npy'):

        print('\tFile type \t: numpy binary file')
        data =  load(file_address)
        print('\tDimension \t:', data.shape)
        return data

    else:
        return 0
###########################################################################

# save numpy array as ibw or txt or npy

def save_data(nparray, format, prefix, dataCategory):

    # nparray : ndarray object
    # format : 0, 1 or 2
    # prefix : prefix file string
    # dataCategory : 'bsl' or 'subt'

    if format == 0:
        file = prefix + "_"+dataCategory+".npy"
        save(file, nparray)
        print("\t saved : ", file)

    if format == 1:
        file = prefix + "_"+dataCategory+".txt"
        savetxt(file, nparray, fmt="%.14f")
        print("\t saved : ", file)

    if format == 2:
        file = prefix + "_"+dataCategory+".ibw"
        wname = prefix+"_"+dataCategory

        wname = wname.rsplit('/', 1)
        trimmed = wname[1]

        # the max length of igor wavename is 31 chars
        if (len(trimmed) > 31):
            print('\t\t Error : igor wave name too long. Saving as igor binary failed.')
            return 1

        wave = IgorWave(nparray, name=trimmed )
        wave.save(file)
        print("\t saved : ", file)
###########################################################################

# derpsalsa

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Wed Jan  8 17:06:53 2020

@author: korepashki

"""
# settings:
global min_struct_el
min_struct_el = 7
max_number_baseline_iterations = 16 # number of iterations in baseline search

#-------------------------------------------------------------------------------

from numpy import round, gradient, mean, exp, max, ones, linspace, sum, convolve, min, ones_like

from itertools import groupby, count

from scipy import stats
from scipy import sparse
from scipy.sparse.linalg import spsolve
from scipy.signal import detrend

######################################################################

import matplotlib.pyplot as plt
plt.style.use('bmh')

plt.rcParams['lines.linewidth'] = 1.8 ;
plt.rc('axes', linewidth=1.50) ;
plt.rcParams.update({'font.size': 13}) ;
#plt.rcParams["font.family"] = "Arial";

######################################################################

def derpsalsa_baseline(x, y, als_lambda, als_p_weight):
    """ asymmetric baseline correction
    Algorithm by Sergio Oller-Moreno et al.
    Parameters which can be manipulated:
    als_lambda  ~ 5e7
    als_p_weight ~ 1.5e-3
    (found from optimization with random 5-point BL)
    """

    # 0: smooth the spectrum 16 times
    #    with the element of 1/100 of the spectral length:
    zero_step_struct_el = int(2*round(len(y)/200) + 1)
    y_sm = molification_smoothing(y, zero_step_struct_el, 16)
    # compute the derivatives:
    y_sm_1d = gradient(y_sm)
    y_sm_2d = gradient(y_sm_1d)
    # weighting function for the 2nd der:
    y_sm_2d_decay = (mean(y_sm_2d**2))**0.5
    weifunc2D = exp(-y_sm_2d**2/2/y_sm_2d_decay**2)
    # weighting function for the 1st der:
    y_sm_1d_decay = (mean((y_sm_1d-mean(y_sm_1d))**2))**0.5
    weifunc1D = exp(-(y_sm_1d-mean(y_sm_1d))**2/2/y_sm_1d_decay**2)

    weifunc = weifunc1D*weifunc2D

    # exclude from screenenig the edges of the spectrum (optional)
    weifunc[0:zero_step_struct_el] = 1; weifunc[-zero_step_struct_el:] = 1

    # estimate the peak height
    peakscreen_amplitude = (max(detrend(y)) - min(detrend(y)))/8 # /8 is good, because this is a characteristic height of a tail
    L = len(y)
    D = sparse.diags([1,-2,1],[0,-1,-2], shape=(L,L-2))
    D = als_lambda * D.dot(D.transpose())
    w = ones(L)
    W = sparse.spdiags(w, 0, L, L)
    # k = 10 * morphological_noise(y) # above this height the peaks are rejected
    for i in range(max_number_baseline_iterations):
        W.setdiag(w)
        Z = W + D
        z = spsolve(Z, w*y)
        w = als_p_weight * weifunc * exp(-((y-z)/peakscreen_amplitude)**2/2) * (y > z) + (1-als_p_weight) * (y < z)
    baseline = z

    subtracted =  y - baseline
    return baseline, subtracted

######################################################################

def molification_smoothing (rawspectrum, struct_el, number_of_molifications):
    """ Molifier kernel here is defined as in the work of Koch et al.:
        JRS 2017, DOI 10.1002/jrs.5010
        The structure element is in pixels, not in cm-1!
        struct_el should be odd integer >= 3
    """
    molifier_kernel = linspace(-1, 1, num=struct_el)
    molifier_kernel[1:-1] = exp(-1/(1-molifier_kernel[1:-1]**2))
    molifier_kernel[0] = 0; molifier_kernel[-1] = 0
    molifier_kernel = molifier_kernel/sum(molifier_kernel)
    denominormtor = convolve(ones_like(rawspectrum), molifier_kernel, 'same')
    smoothline = rawspectrum
    i = 0
    for i in range (number_of_molifications) :
        smoothline = convolve(smoothline, molifier_kernel, 'same') / denominormtor
        i += 1
    return smoothline

###########################################################################

print('\n\t\t ***** USIL-DataAnalysisTools *****')
print('\n\t\t Baseline v1.3 \n\t\tThis is the console output. Welcome\n\t\t')
print('\t\t loading ... ')

############################################################################################

class Ui_mainWindow(object):
    def setupUi(self, mainWindow):
        mainWindow.setObjectName("mainWindow")
        mainWindow.resize(750, 698) # << dimension of main window
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("img\icon_128f.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        mainWindow.setWindowIcon(icon)
        self.label = QtWidgets.QLabel(mainWindow)
        self.label.setGeometry(QtCore.QRect(10, 10, 91, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.line = QtWidgets.QFrame(mainWindow)
        self.line.setGeometry(QtCore.QRect(20, 90, 701, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.pushButton = QtWidgets.QPushButton(mainWindow)
        self.pushButton.setGeometry(QtCore.QRect(130, 10, 231, 31))
        font = QtGui.QFont()
        font.setPointSize(10)

        self.pushButton.setFont(font)
        self.pushButton.setAutoFillBackground(False)
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.press_load_data)

        self.text_dataDetails = QtWidgets.QTextBrowser(mainWindow)
        self.text_dataDetails.setGeometry(QtCore.QRect(140, 110, 301, 31))
        self.text_dataDetails.setObjectName("text_dataDetails")


        self.label_2 = QtWidgets.QLabel(mainWindow)
        self.label_2.setGeometry(QtCore.QRect(20, 110, 101, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.line_2 = QtWidgets.QFrame(mainWindow)
        self.line_2.setGeometry(QtCore.QRect(20, 140, 701, 16))
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")

        #self.label_3 = QtWidgets.QLabel(mainWindow)
        #self.label_3.setGeometry(QtCore.QRect(20, 160, 101, 21))
        #font = QtGui.QFont()
        #font.setPointSize(14)
        #self.label_3.setFont(font)
        #self.label_3.setObjectName("label_3")

        # button to generate baseline ----------------------
        self.genBSL_button = QtWidgets.QPushButton(mainWindow)
        self.genBSL_button.setGeometry(QtCore.QRect(25, 155, 71, 36))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(170, 255, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 255, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(170, 255, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        self.genBSL_button.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(10)
        self.genBSL_button.setFont(font)
        self.genBSL_button.setObjectName("genBSL_button")
        self.genBSL_button.clicked.connect(self.gen_baseline)
        self.ax = None
        # ------------------------------------------------------



        self.pushButton_5 = QtWidgets.QPushButton(mainWindow)
        self.pushButton_5.setGeometry(QtCore.QRect(500, 630, 131, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_5.setFont(font)
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.save_subtracted_ibw)



        self.pushButton_6 = QtWidgets.QPushButton(mainWindow)
        self.pushButton_6.setGeometry(QtCore.QRect(640, 630, 81, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_6.setFont(font)
        self.pushButton_6.setObjectName("pushButton_6")
        self.pushButton_6.clicked.connect(self.save_subtracted_txt)


        self.label_4 = QtWidgets.QLabel(mainWindow)
        self.label_4.setGeometry(QtCore.QRect(360, 640, 141, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")

        self.label_5 = QtWidgets.QLabel(mainWindow)
        self.label_5.setGeometry(QtCore.QRect(30, 640, 81, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")

        self.pushButton_7 = QtWidgets.QPushButton(mainWindow)
        self.pushButton_7.setGeometry(QtCore.QRect(110, 630, 131, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_7.setFont(font)
        self.pushButton_7.setObjectName("pushButton_7")
        self.pushButton_7.clicked.connect(self.save_baseline_ibw)


        self.pushButton_8 = QtWidgets.QPushButton(mainWindow)
        self.pushButton_8.setGeometry(QtCore.QRect(260, 630, 81, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_8.setFont(font)
        self.pushButton_8.setObjectName("pushButton_8")
        self.pushButton_8.clicked.connect(self.save_baseline_txt)

        #self.line_3 = QtWidgets.QFrame(mainWindow)
        #self.line_3.setGeometry(QtCore.QRect(380, 560, 20, 51))
        #self.line_3.setFrameShape(QtWidgets.QFrame.VLine)
        #self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        #self.line_3.setObjectName("line_3")

        self.line_4 = QtWidgets.QFrame(mainWindow)
        self.line_4.setGeometry(QtCore.QRect(10, 530, 711, 16))
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")

        self.label_6 = QtWidgets.QLabel(mainWindow)
        self.label_6.setGeometry(QtCore.QRect(10, 670, 741, 21))
        font = QtGui.QFont()
        font.setFamily("Consolas")
        font.setPointSize(8)
        self.label_6.setFont(font)
        self.label_6.setOpenExternalLinks(True)
        self.label_6.setObjectName("label_6")

        self.line_5 = QtWidgets.QFrame(mainWindow)
        self.line_5.setGeometry(QtCore.QRect(10, 660, 711, 16))
        self.line_5.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")

        self.text_fileAddress = QtWidgets.QTextBrowser(mainWindow)
        self.text_fileAddress.setGeometry(QtCore.QRect(50, 50, 671, 31))
        self.text_fileAddress.setObjectName("text_fileAddress")

        self.plotWidget = MplWidget(mainWindow)
        self.plotWidget.setGeometry(QtCore.QRect(20, 200, 711, 331))
        self.plotWidget.setObjectName("plotWidget")

        self.fileLabel = QtWidgets.QLabel(mainWindow)
        self.fileLabel.setGeometry(QtCore.QRect(470, 120, 251, 16))
        self.fileLabel.setText("")
        self.fileLabel.setObjectName("fileLabel")

        ###########################################
        self.label_7 = QtWidgets.QLabel(mainWindow)
        self.label_7.setGeometry(QtCore.QRect(20, 550, 141, 21))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label_7.setFont(font)
        self.label_7.setObjectName("label_7")

        self.checkBox = QtWidgets.QCheckBox(mainWindow)
        self.checkBox.setGeometry(QtCore.QRect(179, 550, 111, 20))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.checkBox.setFont(font)
        self.checkBox.setObjectName("checkBox")

        self.pushButton_9 = QtWidgets.QPushButton(mainWindow)
        self.pushButton_9.setGeometry(QtCore.QRect(330, 550, 111, 31))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.pushButton_9.setFont(font)
        self.pushButton_9.setObjectName("pushButton_9")
        self.pushButton_9.clicked.connect(self.change_export_directory)

        self.label_8 = QtWidgets.QLabel(mainWindow)
        self.label_8.setGeometry(QtCore.QRect(30, 580, 701, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_8.setFont(font)
        self.label_8.setObjectName("label_8")
        ###########################################

        self.lambda_slider_min = QtWidgets.QLabel(mainWindow)
        self.lambda_slider_min.setGeometry(QtCore.QRect(115, 175, 41, 24))
        self.lambda_slider_min.setObjectName("lambda_slider_min")
        self.lambda_label = QtWidgets.QLabel(mainWindow)
        self.lambda_label.setGeometry(QtCore.QRect(240, 155, 47, 13))
        self.lambda_label.setObjectName("lambda_label")
        self.lambda_box = QtWidgets.QSpinBox(mainWindow)
        self.lambda_box.setGeometry(QtCore.QRect(285, 150, 81, 22))
        self.lambda_box.setMinimum(1000)
        self.lambda_box.setMaximum(500000)
        self.lambda_box.setProperty("value", 75000)
        self.lambda_box.setObjectName("lambda_box")
        self.lambda_slider_max = QtWidgets.QLabel(mainWindow)
        self.lambda_slider_max.setGeometry(QtCore.QRect(395, 175, 41, 24))
        self.lambda_slider_max.setObjectName("lambda_slider_max")
        self.lambda_slider = QtWidgets.QSlider(mainWindow)
        self.lambda_slider.setGeometry(QtCore.QRect(160, 175, 220, 22))
        self.lambda_slider.setMinimum(1000)
        self.lambda_slider.setMaximum(500000)
        self.lambda_slider.setProperty("value", 5.6e4)
        self.lambda_slider.setOrientation(QtCore.Qt.Horizontal)
        self.lambda_slider.setObjectName("lambda_slider")

        ###########################################

        self.p_slider = QtWidgets.QSlider(mainWindow)
        self.p_slider.setGeometry(QtCore.QRect(505, 175, 182, 22))
        self.p_slider.setMinimum(1)
        self.p_slider.setMaximum(1000000)
        self.p_slider.setSingleStep(50)
        self.p_slider.setProperty("value", 1000)
        self.p_slider.setOrientation(QtCore.Qt.Horizontal)
        self.p_slider.setObjectName("p_slider")
        #self.p_label = QtWidgets.QLabel(mainWindow)
        #self.p_label.setGeometry(QtCore.QRect(486, 155, 26, 13))
        #self.p_label.setObjectName("p_label")

        self.p_label_max = QtWidgets.QLabel(mainWindow)
        self.p_label_max.setGeometry(QtCore.QRect(695, 175, 61, 13))
        self.p_label_max.setObjectName("p_label_max")
        self.p_label_min = QtWidgets.QLabel(mainWindow)
        self.p_label_min.setGeometry(QtCore.QRect(460, 175, 34, 13))
        self.p_label_min.setObjectName("p_label_min")

        # top label
        self.p_label_top = QtWidgets.QLabel(mainWindow)
        self.p_label_top.setGeometry(QtCore.QRect(515, 150, 111, 22))
        self.p_label_top.setObjectName("p_label_top")

        ###########################################
        ###########################################

        self.retranslateUi(mainWindow)
        self.lambda_slider.valueChanged['int'].connect(self.lambda_box.setValue)
        self.lambda_box.valueChanged['int'].connect(self.lambda_slider.setValue)
        self.lambda_slider.valueChanged.connect(self.gen_baseline)

        # for p_slider
        self.p_slider.valueChanged['int'].connect(self.update_p_weight)

        QtCore.QMetaObject.connectSlotsByName(mainWindow)

    ############################################################

    def update_p_weight(self):
        '''
        Update the parameters p in the display and rerun the baseline program
        '''
        val = self.p_slider.value()
        scaling_factor = 1e7
        self.p_label_top.setText(" p = " + str(val/scaling_factor) )

        # get updated values and re-evaluate baseline  ----------------
        global param_lambda
        global param_p

        val = self.lambda_slider.value()
        param_lambda = float(val)

        val = self.p_slider.value()
        param_p = float(val) / scaling_factor

        if 'inputData' in globals():
            print('\tParams changed : lambda=', param_lambda, '\t p=', param_p, ' >> Baseline updated')

            xaxis = arange(0, inputData.shape[0],1)
            output = derpsalsa_baseline(xaxis, inputData, als_lambda=param_lambda, als_p_weight=param_p)

            bsl = output[0]
            sub = output[1]

            self.plotWidget.canvas.ax.clear()
            self.plotWidget.canvas.ax.plot(xaxis, inputData, xaxis, bsl,xaxis,sub)
            self.plotWidget.canvas.draw()

            global subtracted_data
            subtracted_data =  sub
            global Baseline
            Baseline = bsl
        else:
            print('\tParams changed : lambda=', param_lambda, '\t p=', param_p, '\t Warning : no input data loaded !')

    ############################################################
    ############################################################

    def gen_baseline(self):
        '''
        Linked to the 'generate baseline button'
        This function reads the current params and runs the
        baseline algorithm.
        Lastly it sets the global variables to the  baseline and subtracted
        data (numpy arrays)
        '''

        global param_lambda
        global param_p

        val = self.lambda_slider.value()
        param_lambda = float(val)

        scaling_factor = 1e7
        val = self.p_slider.value()
        param_p = float(val) / scaling_factor

        if 'inputData' in globals():
            print('\tParams changed : lambda=', param_lambda, '\t p=', param_p, ' >> Baseline updated')

            xaxis = arange(0, inputData.shape[0],1)
            output = derpsalsa_baseline(xaxis, inputData, als_lambda=param_lambda, als_p_weight=param_p)

            bsl = output[0]
            sub = output[1]

            self.plotWidget.canvas.ax.clear()
            self.plotWidget.canvas.ax.plot(xaxis, inputData, xaxis, bsl,xaxis,sub)
            self.plotWidget.canvas.draw()

            global subtracted_data
            subtracted_data =  sub
            global Baseline
            Baseline = bsl
        else:
            print('\tParams changed : lambda=', param_lambda, '\t p=', param_p, '\t Warning : no input data loaded !')

    ############################################################
    ############################################################

    def save_baseline_txt(self):
        '''
        For exporting baseline vector as txt
        '''
        # export baseline
        # array : Baseline
        dir = export_dir
        file = filePrefixStr
        filepath = dir+'/'+file
        save_data(Baseline, 1, filepath, 'bsl')

    def save_baseline_ibw(self):
        '''
        For exporting baseline vector as igor binary wave
        '''
        # export baseline
        # array : Baseline
        dir = export_dir
        file = filePrefixStr
        filepath = dir+'/'+file
        save_data(Baseline, 2, filepath, 'bsl')

    ############################################################

    def save_subtracted_txt(self):
        '''
        For exporting baseline subtracted spectra as txt
        '''
        # export after processing
        # array : subtracted_data
        dir = export_dir
        file = filePrefixStr
        filepath = dir+'/'+file
        save_data(subtracted_data, 1, filepath, 'subt')

    def save_subtracted_ibw(self):
        '''
        For exporting baseline subtracted spectra as igor binary wave
        '''
        # export after processing
        # array : subtracted_data
        dir = export_dir
        file = filePrefixStr
        filepath = dir+'/'+file
        save_data(subtracted_data, 2, filepath, 'subt')

    ############################################################

    # load data and display stats
    def press_load_data(self):

        '''
        To load the data in the program,
        then set the global variables,
        and plot the data
        '''


        file_ext = "*.txt *.ibw *.npy"
        self.file_name = QtWidgets.QFileDialog.getOpenFileName(None, "Open", "", filter=file_ext)



        if self.file_name[0] != '':
            self.text_fileAddress.setText(self.file_name[0])

            print("\t proceeding with file load")
            fileAddress = self.file_name[0]
            print("\t",fileAddress)

            file_string = self.file_name[0]

            extension = file_string.split(".")[-1]
            print('\t file extension : ', extension)

            filePrefix = file_string.rsplit('/', 1)
            inpDirectory = filePrefix[0]

            filePrefix_noExt = filePrefix[1].split(".")
            #print('\t >> ', filePrefix_noExt[0])
            print ('\t >> input directory \t: ', inpDirectory)

            global filePrefixStr
            filePrefixStr = filePrefix_noExt[0]

            self.fileLabel.setText(" File prefix : "+" " + filePrefix_noExt[0])


            data = load_data(fileAddress)

            if type(data) != ndarray:
                print('\t Error \t: Incorrect file selected.')
                message=" Error : Incorrect file selected. (.ibw, .txt, .npy)"
                self.text_dataDetails.setText(message)

            else:
                # data details for display
                dimension = data.ndim
                #print("\tdim \t: ", dimension)
                nRows=data.shape[0]

                message=" Dimension : "+str(nRows)+" x "+str(dimension)

                if (dimension > 1):
                    message = message + " (Plotting first column)"

                self.text_dataDetails.setText(message)
                self.checkBox.setChecked(True)

                global export_dir
                export_dir = inpDirectory
                self.label_8.setText('Export dir : '+export_dir)



                # plot
                xaxis = arange(0, data.shape[0], 1)

                # new lines added tp test
                self.plotWidget.canvas.ax.clear()
                self.plotWidget.canvas.draw()

                if (dimension > 1):
                    y = data[:,0]
                else:
                    y=data

                self.plotWidget.canvas.ax.plot(xaxis, y)
                self.plotWidget.canvas.draw()

                global inputData
                inputData = y

    ############################################################

    # load data and display stats
    def change_export_directory(self):
        '''
        Linked to button, this functions allows one to change the export directory
        '''

        self.checkBox.setChecked(False)

        self.selected_dir = QtWidgets.QFileDialog.getExistingDirectory(None, 'Select Folder')

        print('\t>> destination dir : ', self.selected_dir)
        global export_dir
        export_dir = self.selected_dir
        self.label_8.setText('Export dir : '+export_dir)

    ############################################################

    def retranslateUi(self, mainWindow):
        _translate = QtCore.QCoreApplication.translate
        mainWindow.setWindowTitle(_translate("mainWindow", "USIL-DataAnalysisTools : Baseline"))
        self.label.setText(_translate("mainWindow", "Load data"))
        self.pushButton.setText(_translate("mainWindow", "Load data : igor binary | txt |  npy"))
        self.label_2.setText(_translate("mainWindow", "Data details"))
        #self.label_3.setText(_translate("mainWindow", "Process")) ## <<
        self.genBSL_button.setText(_translate("mainWindow", "generate\nbaseline"))

        self.pushButton_5.setText(_translate("mainWindow", "export as igor binary"))
        self.pushButton_6.setText(_translate("mainWindow", "export as txt"))
        self.label_4.setText(_translate("mainWindow", "Subtracted data"))
        self.label_5.setText(_translate("mainWindow", "Baseline"))
        self.pushButton_7.setText(_translate("mainWindow", "export as igor binary"))
        self.pushButton_8.setText(_translate("mainWindow", "export as txt"))
        self.label_6.setText(_translate("mainWindow", "<html><head/><body><p>USIL-DataAnalysisTools : Baseline, v1.2, Ankit Raj | Ref. <a href=\"https://analyticalsciencejournals.onlinelibrary.wiley.com/doi/10.1002/jrs.5952\"><span style=\" text-decoration: underline; color:#0000ff;\">10.1002/jrs.5952</span></a> by Vitaly I. Korepanov</p></body></html>"))
        self.label_7.setText(_translate("mainWindow", "Export directory"))
        self.checkBox.setText(_translate("mainWindow", "same as source"))
        self.pushButton_9.setText(_translate("mainWindow", "Change export dir"))
        self.label_8.setText(_translate("mainWindow", "Export dir :"))

        self.lambda_slider_min.setText(_translate("mainWindow", "curved"))
        self.lambda_label.setText(_translate("mainWindow", "Lambda"))
        self.lambda_slider_max.setText(_translate("mainWindow", "straight\nline"))
        #self.p_label.setText(_translate("mainWindow", "P"))
        self.p_label_max.setText(_translate("mainWindow", "wavy"))
        self.p_label_min.setText(_translate("mainWindow", "smooth"))
        self.p_label_top.setText(_translate("mainWindow", "p = "))


if hasattr(QtCore.Qt, 'AA_EnableHighDpiScaling'):
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)

if hasattr(QtCore.Qt, 'AA_UseHighDpiPixmaps'):
    QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('Fusion')
    mainWindow = QtWidgets.QDialog()
    ui = Ui_mainWindow()
    ui.setupUi(mainWindow)
    mainWindow.show()
    sys.exit(app.exec_())

#####################################################################
